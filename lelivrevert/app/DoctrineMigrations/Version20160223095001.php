<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160223095001 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE book CHANGE title title VARCHAR(255) DEFAULT NULL, CHANGE subtitle subtitle VARCHAR(255) DEFAULT NULL, CHANGE EAN EAN VARCHAR(255) DEFAULT NULL, CHANGE author author VARCHAR(255) DEFAULT NULL, CHANGE editor editor VARCHAR(255) DEFAULT NULL, CHANGE language language VARCHAR(255) DEFAULT NULL, CHANGE cover cover VARCHAR(255) DEFAULT NULL, CHANGE pages_nb pages_nb VARCHAR(255) DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE book CHANGE title title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE subtitle subtitle VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE EAN EAN VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE author author VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE editor editor VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE language language VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE cover cover VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE pages_nb pages_nb VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE description description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
