<?php

namespace LelivrevertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * APIDebugQueue
 *
 * @ORM\Table(name="a_p_i_debug_queue")
 * @ORM\Entity(repositoryClass="LelivrevertBundle\Repository\APIDebugQueueRepository")
 */
class APIDebugQueue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="marketplace", type="integer")
     */
    private $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="endpoint", type="text")
     */
    private $endpoint;

    /**
     * @var string
     *
     * @ORM\Column(name="request", type="text")
     */
    private $request;

    /**
     * @var string
     *
     * @ORM\Column(name="response", type="text")
     */
    private $response;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param integer $marketplace
     * @return APIDebugQueue
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return integer 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set endpoint
     *
     * @param string $endpoint
     * @return APIDebugQueue
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Get endpoint
     *
     * @return string 
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Set request
     *
     * @param string $request
     * @return APIDebugQueue
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return string 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set response
     *
     * @param string $response
     * @return APIDebugQueue
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return string 
     */
    public function getResponse()
    {
        return $this->response;
    }
}
