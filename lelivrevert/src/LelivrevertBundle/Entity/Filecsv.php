<?php

namespace LelivrevertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Filecsv
 *
 * @ORM\Table(name="filecsv")
 * @ORM\Entity(repositoryClass="LelivrevertBundle\Repository\FilecsvRepository")
 */
class Filecsv
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the file as a csv file.")
     * @Assert\File(mimeTypes={ "text/plain" })
     */
    private $csvFile;

    public function getCsvFile()
    {
        return $this->csvFile;
    }

    public function setCsvFile($csvFile)
    {
        $this->csvFile = $csvFile;

        return $this;
    }


}
