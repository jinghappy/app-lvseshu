<?php

namespace LelivrevertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * APIActionQueue
 *
 * @ORM\Table(name="a_p_i_action_queue")
 * @ORM\Entity(repositoryClass="LelivrevertBundle\Repository\APIActionQueueRepository")
 */
class APIActionQueue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="marketplace", type="integer")
     */
    private $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param integer $marketplace
     * @return APIActionQueue
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return integer 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return APIActionQueue
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return APIActionQueue
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return APIActionQueue
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }
}
