<?php

namespace LelivrevertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductID
 *
 * @ORM\Table(name="product_i_d")
 * @ORM\Entity(repositoryClass="LelivrevertBundle\Repository\ProductIDRepository")
 */
class ProductID
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="marketplace", type="integer")
     */
    private $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="prid", type="string", length=255)
     */
    private $prid;

    /**
     * @var string
     *
     * @ORM\Column(name="ean", type="string", length=255)
     */
    private $ean;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param integer $marketplace
     * @return ProductID
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return integer 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set prid
     *
     * @param string $prid
     * @return ProductID
     */
    public function setPrid($prid)
    {
        $this->prid = $prid;

        return $this;
    }

    /**
     * Get prid
     *
     * @return string 
     */
    public function getPrid()
    {
        return $this->prid;
    }

    /**
     * Set ean
     *
     * @param string $ean
     * @return ProductID
     */
    public function setEan($ean)
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * Get ean
     *
     * @return string 
     */
    public function getEan()
    {
        return $this->ean;
    }
}
