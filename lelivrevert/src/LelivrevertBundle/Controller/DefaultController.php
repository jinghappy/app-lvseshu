<?php

namespace LelivrevertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LelivrevertBundle:Default:index.html.twig');
    }
}
