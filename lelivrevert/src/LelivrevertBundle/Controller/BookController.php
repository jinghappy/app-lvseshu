<?php

namespace LelivrevertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;


use LelivrevertBundle\Entity\Book;
use LelivrevertBundle\Entity\Filecsv;
use LelivrevertBundle\Entity\Category;
use LelivrevertBundle\Form\BookType;



class BookController extends Controller
{
    public function indexAction()
    {
        $books = $this->getDoctrine()
            ->getRepository('LelivrevertBundle:Book')->findAll();

        $title='Liste des livres';

        return $this->render('LelivrevertBundle:Book:index.html.twig', array(

            'books' => $books, 'title'=>$title

        ));

    }

    public function add_multipleAction()
    {
        return $this->render('LelivrevertBundle:Book:add.multiple.html.twig', array(
            // ...
        ));
    }

    public function addAction(Request $request)
    {
        $title='Ajouter un livre';
        $book=new Book();

        //$form=$this->createForm(BookType::class, $book);

        $form=$this->createForm(new BookType($this->getDoctrine()->getManager()), $book);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em=$this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();

            return $this->redirectToRoute('index');

        }


        return $this->render('LelivrevertBundle:Book:add.html.twig', array(
            'form' => $form->createView(),
            'title'=>$title
        ));
    }

    public function editAction()
    {
        return $this->render('LelivrevertBundle:Book:edit.html.twig', array(
            // ...
        ));
    }

    public function deleteAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $book=$em->getRepository('LelivrevertBundle:Book')->find($id);
        $em->remove($book);
        $em->flush();

        return $this->redirectToRoute('index');
    }

    public function displayAction($id)
    {

        $book = $this->getDoctrine()
            ->getRepository('LelivrevertBundle:Book')->find($id);

        $title='Afficher un livre';

        return $this->render('LelivrevertBundle:Book:display.html.twig', array(
            'book' => $book, 'title'=>$title
        ));
    }

    public function upload_csvAction(Request $request)
    {
        $title='Ajouter des livres (import CSV)';

        $filecsv= new Filecsv();
        $form= $this->createFormBuilder($filecsv)
            ->add('csvFile', FileType::class , array('label' => 'CSV File'))
            ->add('submit', SubmitType::class, array('label' =>'Envoyer'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isValid())
        {
            $file = $filecsv->getCsvFile();

            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            $csvDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/csv';

            $f=$file->move($csvDir, $fileName);

            $filecsv->setCsvFile($fileName);

            $fp = fopen($f,"r");

            $arr_ean=[];
            while(! feof($fp))
            {
                $ean=explode(';',fgetcsv($fp)[0]);

                array_push($arr_ean,$ean[0]);
            }

            fclose($fp);

            foreach ($arr_ean as $code_ean) {
                $request = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' . $code_ean;
                $response = file_get_contents($request);
                $results = json_decode($response);
                if($code_ean){
                    if ($results->totalItems != 0) {
                        $livre=new Book;
                        $book = $results->items;

                        $livre->setTitle($book[0]->volumeInfo->title);
                        if(isset($book[0]->volumeInfo->subtitle)){
                            $livre->setSubtitle($book[0]->volumeInfo->subtitle);
                        }

                        $livre->setEAN($code_ean);
                        $livre->setAuthor($book[0]->volumeInfo->authors[0]);
                        $livre->setEditor($book[0]->volumeInfo->publisher);
                        $livre->setLanguage($book[0]->volumeInfo->language);

                        if(isset($book[0]->volumeInfo->imageLinks)){
                            $livre->setCover($book[0]->volumeInfo->imageLinks->thumbnail);
                        }

                        $livre->setPagesNb($book[0]->volumeInfo->pageCount);
                        if(isset($book[0]->volumeInfo->description)){
                            $livre->setDescription($book[0]->volumeInfo->description);
                        }

                        if(isset($book[0]->volumeInfo->categories[0])){

                            $repository = $this->getDoctrine()->getManager()->getRepository('LelivrevertBundle:Category');
                            $category=$repository->findOneByName($book[0]->volumeInfo->categories[0]);

                            if ($category == null) {
                                $category = new Category();
                                $category->setName($book[0]->volumeInfo->categories[0]);
                                $em= $this->getDoctrine()->getManager();
                                $em->persist($category);
                                $em->flush();
                            }
                            $livre->setCategory($category);
                        }

                        $em=$this->getDoctrine()->getManager();
                        $em->persist($livre);
                        $em->flush();
                    }
                }

            }

            return $this->redirectToRoute('index');


        }


        return $this->render('LelivrevertBundle:Book:upload.csv.html.twig', array(
            'form' => $form->createView(),'title'=>$title
        ));

    }

    public function form_addAction()
    {



        return $this->render('LelivrevertBundle:Book:form.add.html.twig', array(
            // ...
        ));
    }

    public function form_editAction($id, Request $request)
    {
        $title='Éditer un livre';
        $em=$this->getDoctrine()->getManager();
        $book = $em->getRepository('LelivrevertBundle:Book')->find($id);

        $livre=new Book();

        $form=$this->createFormBuilder($livre)
            ->add('title', TextType::class, array('required'=>false, 'data' =>$book->getTitle()))
            ->add('subtitle', TextType::class, array('required'=>false, 'data' =>$book->getSubtitle()))
            ->add('author', TextType::class, array('required'=>false, 'data' => $book->getAuthor(),))
            ->add('editor', TextType::class, array('required'=>false, 'data' => $book->getEditor(),))
            ->add('language', TextType::class, array('required'=>false, 'data' => $book->getLanguage(),))
            ->add('pages_nb', TextType::class, array('required'=>false, 'data' => $book->getPagesNb()))
            ->add('ean', TextType::class, array('required'=>false, 'data' => $book->getEAN()))
            //->add('cover', HiddenType::class, array('required'=>false, 'placeholder' => $book,))
            ->add('description', TextareaType::class, array('required'=>false, 'data' => $book->getDescription()))
            ->add('save', SubmitType::class, array('label' => 'Soumettre'))
            ->getForm();

        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){

            $book->setTitle($form->get('title')->getData());
            $book->setSubtitle($form->get('subtitle')->getData());
            $book->setEAN($form->get('ean')->getData());
            $book->setAuthor($form->get('author')->getData());
            $book->setEditor($form->get('editor')->getData());
            $book->setLanguage($form->get('language')->getData());
            $book->setPagesNb($form->get('pages_nb')->getData());
            $book->setDescription($form->get('description')->getData());

            $em->flush();

            return $this->redirectToRoute('index');

        }



        return $this->render('LelivrevertBundle:Book:form.edit.html.twig', array(
            'form' => $form->createView(),'title'=>$title
        ));
    }



    public function form_deleteAction()
    {


        return $this->render('LelivrevertBundle:Book:form.delete.html.twig', array(

        ));
    }




    public function isbn_jsonAction($isbn)
    {


        $request = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' . $isbn;
        $response = file_get_contents($request);
        $results = json_decode($response);

        if($results->totalItems == 0){

            return new Response('Livre introuvable');
        }
        $book = $results->items;

        return $this->render('LelivrevertBundle:Book:isbntojson.html.twig', array(
            'book'=>$book
        ));
    }


}
