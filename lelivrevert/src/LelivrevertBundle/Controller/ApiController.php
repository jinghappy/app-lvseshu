<?php

namespace LelivrevertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;


use LelivrevertBundle\Entity\Book;
use LelivrevertBundle\Entity\Filecsv;
use Symfony\Component\HttpFoundation\JsonResponse;


class ApiController extends Controller
{

  public function ean_jsonAction($ean)
  {
    $aws_access_key_id = "AKIAJ7HZTZSMVQWIQ5UQ";
    $aws_secret_key = "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";
    $endpoint = "webservices.amazon.com";
    $uri = "/onca/xml";
    $params = array(
        "Service" => "AWSECommerceService",
        "Operation" => "ItemLookup",
        "AWSAccessKeyId" => "AKIAJ7HZTZSMVQWIQ5UQ",
        "AssociateTag" => "lewebfrancais-21",
        "ItemId" => $ean,
        "IdType" => "EAN",
        "ResponseGroup" => "Images",
        "SearchIndex" => "Books"
    );

    if (!isset($params["Timestamp"])) {
      $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
    }

    ksort($params);
    $pairs = array();
    foreach ($params as $key => $value) {
      array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
    }
    $canonical_query_string = join("&", $pairs);
    $string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;
    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
    $request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);
    $xml_string=file_get_contents($request_url);
    $xml_object=simplexml_load_string($xml_string);

    $request = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' . $ean;
    $response = file_get_contents($request);
    $results = json_decode($response);

    if($results->totalItems == 0){

      return new JsonResponse(null);
    }

    $book = $results->items;
    if(isset($book[0]->volumeInfo->title)){
        $title=$book[0]->volumeInfo->title;
    }else{
      $title=null;
    }

    if(isset($book[0]->volumeInfo->subtitle)){
        $subtitle=$book[0]->volumeInfo->subtitle;
    }else{
      $subtitle=null;
    }

    if(isset($book[0]->volumeInfo->authors[0])){
        $authors=$book[0]->volumeInfo->authors[0];
    }else {
      $authors=null;
    }

    if(isset($book[0]->volumeInfo->publisher)){
        $editor=$book[0]->volumeInfo->publisher;
    }else {
      $editor=null;
    }

    if(isset($book[0]->volumeInfo->language)){
        $language=$book[0]->volumeInfo->language;
    }else {
      $language=null;
    }

    if(isset($book[0]->volumeInfo->imageLinks->thumbnail)){
        $image=$book[0]->volumeInfo->imageLinks->thumbnail;
    }else {
      $image=$xml_object->Items->Item->LargeImage->URL;
      $image=(array)($image);
      $image=$image[0];
    }

    if(isset($book[0]->volumeInfo->pageCount)){
        $pagesNb=$book[0]->volumeInfo->pageCount;
    }else {
      $pagesNb=null;
    }


    if(isset($book[0]->volumeInfo->categories[0])){
        $category=$book[0]->volumeInfo->categories[0];
    }else {
      $category=null;
    }



    if(isset($book[0]->volumeInfo->description)){
        $description=$book[0]->volumeInfo->description;
    }else {
      $description=null;
    }

    $data = array(
            'title' => $title,
            'subtitle' => $subtitle,
            'authors' => $authors,
            'editor' => $editor,
            'language' => $language,
            'image' => $image,
            'pagesNb' => $pagesNb,
            'categories' => $category,
            'description' => $description
        );



    return new JsonResponse($data);
  }


  public function queryAction($par, $contenu){


    $repository=$this->getDoctrine()->getRepository('LelivrevertBundle:Book');

    $qb=$repository->createQueryBuilder('b');

    switch ($par) {

      case 'ean':

        $qb->where($qb->expr()->orX(
            $qb->expr()->eq('b.eAN', ':EAN')
        ));
        $qb->setParameter('EAN', $contenu);



        break;

      case 'soustitre':

        $qb->where($qb->expr()->orX(
            $qb->expr()->like('b.subtitle', ':subtitle')
        ));
        $qb->setParameter('subtitle', '%'.$contenu.'%');

        break;

      case 'auteur':

        $qb->where($qb->expr()->orX(
            $qb->expr()->like('b.author', ':author')
        ));
        $qb->setParameter('author', '%'.$contenu.'%');

        break;


      case 'titre':

        $qb->where($qb->expr()->orX(
            $qb->expr()->like('b.title', ':title')
        ));
        $qb->setParameter('title', '%'.$contenu.'%');


        break;

      case 'editeur':
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('b.editor', ':editor')
        ));
        $qb->setParameter('editor', '%'.$contenu.'%');

        break;

      case 'description':
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('b.description', ':description')
        ));
        $qb->setParameter('description', '%'.$contenu.'%');

        break;

    }

    $query=$qb->getQuery();

    $books = $query->getResult();

    if($books) {

      $id=$books[0]->getId();

      if ($books[0]->getTitle()) {
        $title = $books[0]->getTitle();
      } else {
        $title = null;
      }

      if ($books[0]->getEAN()) {
        $ean = $books[0]->getEAN();
      } else {
        $ean = null;
      }

      if ($books[0]->getAuthor()) {
        $authors = $books[0]->getAuthor();
      } else {
        $authors = null;
      }


      if ($books[0]->getEditor()) {
        $editor = $books[0]->getEditor();
      } else {
        $editor = null;
      }


      $data = array(
          'id'=>$id,
          'title' => $title,
          'ean' => $ean,
          'authors' => $authors,
          'editor' => $editor

      );
    }else{
      $data=null;
    }

    return new JsonResponse($data);

  }


  public function amazonAction($ean)
  {
    $aws_access_key_id = "AKIAJ7HZTZSMVQWIQ5UQ";
    $aws_secret_key = "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";
    $endpoint = "webservices.amazon.com";
    $uri = "/onca/xml";
    $params = array(
        "Service" => "AWSECommerceService",
        "Operation" => "ItemLookup",
        "AWSAccessKeyId" => "AKIAJ7HZTZSMVQWIQ5UQ",
        "AssociateTag" => "lewebfrancais-21",
        "ItemId" => $ean,
        "IdType" => "EAN",
        "ResponseGroup" => "Images",
        "SearchIndex" => "Books"
    );

    if (!isset($params["Timestamp"])) {
      $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
    }

    ksort($params);

    $pairs = array();

    foreach ($params as $key => $value) {
      array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
    }
    $canonical_query_string = join("&", $pairs);
    $string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;
    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

    $request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

    $xml_string=file_get_contents($request_url);
    $xml_object=simplexml_load_string($xml_string);
    return new Response($xml_object->Items->Item->LargeImage->URL);

  }

}


