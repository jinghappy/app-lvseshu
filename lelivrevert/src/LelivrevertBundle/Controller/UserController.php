<?php

namespace LelivrevertBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use LelivrevertBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UserController extends Controller
{
    public function showAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('LelivrevertBundle:User')->findAll();

        $title='Liste des utilisateurs';

        return $this->render('LelivrevertBundle:User:show.html.twig', array(
            'users' => $users, 'title'=>$title
        ));
    }

    public function addAction(Request $request)
    {
        $title='Ajouter un utilisateur';

        $user=new User();
        $form=$this->createFormBuilder($user)
            ->add('username',TextType::class, array('label'=>'Nom'))
            ->add('email', EmailType::class, array('label'=>'Adresse e-mail'))
            ->add('password', PasswordType::class, array('label'=>'Mot de passe'))
            ->add('save', SubmitType::class, array('label'=>'Soumettre'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em=$this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_show');

        }


        return $this->render('LelivrevertBundle:User:add.html.twig', array(
             'title'=>$title, 'form'=>$form->createView()
        ));
    }


    public function editAction(Request $request,$id)
    {
        $title='Éditer un utilisateur';

        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('LelivrevertBundle:User')->find($id);

        $userforform=new User();

        $form=$this->createFormBuilder($userforform)
            ->add('username',TextType::class, array('label'=>'Nom'))
            ->add('email', EmailType::class, array('label'=>'Adresse e-mail'))
            ->add('password', PasswordType::class, array('label'=>'Mot de passe'))
            ->add('save', SubmitType::class, array('label'=>'Soumettre'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $user->setUsername($form->get('username')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setPassword($form->get('password')->getData());

            $em->flush();
            return $this->redirectToRoute('user_show');

        }


        return $this->render('LelivrevertBundle:User:edit.html.twig', array(
            'title'=>$title, 'form'=>$form->createView()
        ));
    }

}
