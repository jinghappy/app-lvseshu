<?php

namespace LelivrevertBundle\Controller;


use LelivrevertBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class CategoryController extends Controller
{
    public function showAction()
    {

        $categories = $this->getDoctrine()
            ->getRepository('LelivrevertBundle:Category')->findAll();

        $title='Liste des catégories';

        return $this->render('LelivrevertBundle:Category:show.html.twig', array(
            'categories' => $categories, 'title'=>$title
        ));
    }

    public function addAction(Request $request)
    {
        $title='Ajouter une catégorie';
        $cat=new Category();

        $form=$this->createFormBuilder($cat)
            ->add('name', TextType::class,array('label'=>'Nom'))
            ->add('save', SubmitType::class, array('label'=>'Envoyer'))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($cat);
            $em->flush();
            return $this->redirectToRoute('category_show');

        }
        return $this->render('LelivrevertBundle:Category:add.html.twig', array(
            'title'=>$title, 'form'=>$form->createView()
        ));
    }

    public function deleteAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $cat=$em->getRepository('LelivrevertBundle:Category')->find($id);
        $em->remove($cat);
        $em->flush();

        return $this->redirectToRoute('category_show');
    }

}
