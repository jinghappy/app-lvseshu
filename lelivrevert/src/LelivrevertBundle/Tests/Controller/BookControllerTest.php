<?php

namespace LelivrevertBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/book');
    }

    public function testAdd_multiple()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/add_multiple');
    }

    public function testAdd()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/add');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/edit');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/delete');
    }

    public function testDisplay()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/display');
    }

    public function testUpload_csv()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/upload_csv');
    }

    public function testForm_add()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/form_add');
    }

    public function testForm_edit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/form_edit');
    }

    public function testForm_delete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/form_delete');
    }

}
