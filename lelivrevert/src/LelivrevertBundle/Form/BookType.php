<?php

namespace LelivrevertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use LelivrevertBundle\Entity\Category;

class BookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder
            ->add('title', TextType::class, array('required'=>false))
            ->add('subtitle', TextType::class, array('required'=>false))
            ->add('author', TextType::class, array('required'=>false))
            ->add('editor', TextType::class, array('required'=>false))
            ->add('language', TextType::class, array('required'=>false))
            ->add('pages_nb', TextType::class, array('required'=>false))
            ->add('EAN', TextType::class, array('required'=>false))
            ->add('cover', HiddenType::class, array('required'=>false))
            ->add('description', TextareaType::class, array('required'=>false))
            ->add('category', TextType::class, array('required'=>false))
            ->add('save', SubmitType::class, array('label' => 'Soumettre'))


            ->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $evt) {
                $book = $evt->getData();
                $repository = $this->em
                    ->getRepository('LelivrevertBundle:Category');
                $category = $repository->findOneByName($book['category']);
                if ($category == null) {
                    $category = new Category();
                    $category->setName($book['category']);
                    $this->em->persist($category);
                    $this->em->flush();
                }
                $book['category'] = $category;
                $evt->setData($book);
            });
    }




    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LelivrevertBundle\Entity\Book'
        ));
    }
}
